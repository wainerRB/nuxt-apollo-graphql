import 'isomorphic-fetch'
import ApolloClient, { createNetworkInterface, addTypename } from 'apollo-client'

const apolloClient = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: 'https://api.graph.cool/simple/v1/ciwce5xw82kh7017179gwzn7q',
    transportBatching: true,
  }),
  queryTransformer: addTypename,
  dataIdFromObject: r => r.id,
})

export default apolloClient
