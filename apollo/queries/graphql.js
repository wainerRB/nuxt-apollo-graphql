import gql from 'graphql-tag'


export const GET_USERS = gql `
  query GETUserQuery
    {
      users {
        _id,
        email
      }
  }
`

export const SAVE_USER = gql `
  mutation CreateUserQuery($name: String!, $email: String!) {
      createUser (name: $name, email: $email) {
      _id
      name
      email
    }
  }
`
